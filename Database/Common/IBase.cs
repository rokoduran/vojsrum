﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Database.Common
{
    public interface IBase
    {
        [ScaffoldColumn(false)] Int64 ID { get; set; }

        [Required] DateTime DateCreated { get; set; }
        [Required] DateTime DateUpdated { get; set; }
    }
}
