using System;
using Database.Common;
using System.ComponentModel.DataAnnotations;

namespace Database.ViewModels
{
    public class UserView: IBase{
        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }

        public string Username {get; set;}
        
        public string Mail { get; set; }

        public string Password { get; set; }

        //Link to User's profile picture
        public string ProfilePictureLink { get; set; }
    }
}