using System;
using Database.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Database.ViewModels
{
    public class TextChannelView: IBase{
        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }

        //Link to the text channel's banner
        public string BannerLink { get; set; }
        //The Text channel's description
        public string Description { get; set; }

        //The messages in the text channel
        public ICollection<MessageView> Messages { get; set; }
 
        //Name of the text channel
        public string Name { get; set; }
        
        //The room the text channel belongs to
        public Int64 RoomId { get; set; }
    }
    
}