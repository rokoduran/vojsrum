using System;
using Database.Common;
using System.ComponentModel.DataAnnotations;

namespace Database.ViewModels
{
    public class VoiceChannelView: IBase{
        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }

        [Required]public DateTime DateUpdated { get; set; }        
        //Name of the voice channel
        public string Name { get; set; }
        //The room the voice channel belongs to
        public Int64 RoomId { get; set; }
    }
    
}