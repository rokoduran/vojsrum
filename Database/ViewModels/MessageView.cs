using System;
using Database.Common;
using Database.Models;
using System.ComponentModel.DataAnnotations;

namespace Database.ViewModels
{
    public class MessageView: IBase{
        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }

        [Required]public DateTime DateUpdated { get; set; }
        //The user ID of the poster
        public Int64 UserId { get; set; }

        //Text of the message
        public string MessageData { get; set; }
        //Type of message
        public MessageType MessageType { get; set; }

        //The channel ID the message was posted in
        public Int64 TextChannelId { get; set; }
    }
    
}