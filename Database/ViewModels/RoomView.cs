using System;
using Database.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Database.ViewModels
{
    public class RoomView: IBase{
        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }
        //Name of the room
        public string Name { get; set; }

        public ICollection<TextChannelView> TextChannels { get; set; }

        public ICollection<VoiceChannelView> VoiceChannels { get; set;}
    }
    
}