using Microsoft.EntityFrameworkCore;
using Database.Models;

namespace Database.Context
{
    public class VojsrumDbContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=SHAENLIR\\SQLEXPRESS;Database=VojsrumDB;Trusted_Connection=True;");
        }
        public DbSet<Room> Room { get; set; }

        public DbSet<Message> Message { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<RoomUser> RoomUser { get; set; }

        public DbSet<TextChannel> TextChannel { get; set; }

        public DbSet<VoiceChannel> voiceChannel { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<RoomUser>()
                .HasKey(x => new { x.RoomId, x.UserId });
        }
    }
}
