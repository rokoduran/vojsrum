using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Database.Common;

namespace Database.Models
{
    public class User : IBase
    {
        public User(){
            Rooms = new List<RoomUser>();
        }

        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }
        public string Username {get; set;}
        public string Mail { get; set; }

        public string Password { get; set; }

        //Link to User's profile picture
        public string ProfilePictureLink { get; set; }

        public ICollection<RoomUser> Rooms { get; set;}
    }
}