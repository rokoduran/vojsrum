namespace Database.Models{
    public enum MessageType{
        Text = 0,
        Hyperlink,
        Multimedia
    }
}