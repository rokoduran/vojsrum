namespace Database.Models{
    public enum UserRole{
        Admin = 0,
        Moderator,
        Member,
        Guest,
        Banned,
    }
}