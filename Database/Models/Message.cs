
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Database.Common;

namespace Database.Models{
    public class Message: IBase
    {
        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }
        //Who posted the message
        public Int64 UserId { get; set; }
        
        public User User { get; set; }

        //Text of the message
        public string MessageData { get; set; }
        //Type of message
        public MessageType MessageType { get; set; }

        //The channel the message was posted in
        public Int64 TextChannelId { get; set; }

        public TextChannel TextChannel { get; set; }
    }
}