using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Database.Models
{
    public class RoomUser
    {
        public Int64 UserId { get; set; }
        public User user {get; set;}
        public Int64 RoomId { get; set; }
        public Room Room { get; set;}
        public UserRole UserRole { get; set; }
    }
}
