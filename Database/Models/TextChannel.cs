using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Database.Common;

namespace Database.Models{
    public class TextChannel: IBase
    {
        public TextChannel(){
            Messages = new List<Message>();
        }
        [ScaffoldColumn(false)]public Int64 ID { get; set; }
        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }
        //Link to the text channel's banner
        public string BannerLink { get; set; }
        //The Text channel's description
        public string Description { get; set; }
        //Name of the text channel
        public string Name { get; set; }
        //The messages in the text channel
        public ICollection<Message> Messages { get; set; }
        //The room the text channel belongs to
        public Int64 RoomId { get; set; }

        public Room Room {get; set;}
        
    }
}