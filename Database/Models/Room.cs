using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Database.Common;

namespace Database.Models
{
public class Room: IBase
    {
        public Room(){
            RoomUsers = new List<RoomUser>();
            TextChannels = new List<TextChannel>();
            VoiceChannels = new List<VoiceChannel>();
        }

        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }
        //Room name
        public string Name { get;  set; }

        public ICollection<RoomUser> RoomUsers {get; set;}

        public ICollection<TextChannel> TextChannels {get; set;}

        public ICollection<VoiceChannel> VoiceChannels {get; set;}


    };
}