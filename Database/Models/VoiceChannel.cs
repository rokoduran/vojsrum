using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using Database.Common;

namespace Database.Models{
    public class VoiceChannel: IBase
    {
        [ScaffoldColumn(false)]public Int64 ID { get; set; }

        [Required]public DateTime DateCreated { get; set; }
        [Required]public DateTime DateUpdated { get; set; }
        public string Name { get; set; }
        //The room the voice channel belongs to
        public Int64 RoomId { get; set; }

        public Room Room { get; set; }
    }
}