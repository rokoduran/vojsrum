using System;
using System.Collections.Generic;
using System.Text;
using Database.ViewModels;
using System.Threading.Tasks;
using Repository;
using Database.Models;

namespace Service
{
    public class UserService : BaseService<UserRepository, User, UserView>
    {
        public UserService(UserRepository repository) : base(repository)
        {
        }

        public async Task<UserView> GetByUsernameAsync(string username){
            return await m_repository.GetByUsernameAsync(username);
        }

        public async Task<IEnumerable<UserView>> GetUsersInRoomAsync(Int64 roomId ){
            return await m_repository.GetUsersInRoomAsync(roomId);
        }


        public async Task<UserRole> GetUserRoleInRoomAsync(Int64 userId, Int64 roomId){
            return await m_repository.GetUserRoleInRoomAsync(userId, roomId);
        }

        public async Task BanUserInRoomAsync(Int64 userId, Int64 roomId){
            await m_repository.BanUserInRoomAsync(userId, roomId);
        }
        public async Task PromoteUserInRoomAsync(Int64 userId, Int64 roomId){
            await m_repository.PromoteUserInRoomAsync(userId, roomId);
        }

        public async Task<bool> AuthenticateUserAsync(string username,string password){
            return await m_repository.AuthenticateUserAsync(username, password);
        }
    }
}