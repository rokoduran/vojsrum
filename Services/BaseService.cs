using System;
using System.Collections.Generic;
using System.Text;
using Database.Common;
using System.Threading.Tasks;
using Repository;
using Database.Models;

namespace Service
{
    public abstract class BaseService<TRepository,TData,TDomain> 
    where TData : class,IBase
    where TDomain : class,IBase
    where TRepository : BaseRepository<TData,TDomain>
    {
        protected readonly TRepository m_repository;

        public BaseService(TRepository repository) => m_repository = repository;

        public async Task<bool> DeleteAsync(Int64 id)
        {
            return await m_repository.DeleteAsync(id);
        }

       /* public async Task<IEnumerable<TType>> GetAllForParamsAsync(FilteringParam filter, PagingParam paging, SortingParam sort)
        {
           return (await Repository.GetAllForParamsAsync(filter, paging, sort));
        }*/

        public async Task<TDomain> GetByIDAsync(Int64 id)
        {
            return await m_repository.GetByIDAsync(id);
        }

        public async Task<int> GetCountAsync()
        {
            return await m_repository.GetCountAsync();
        }

        public async Task<TDomain> InsertAsync(TDomain insert)
        {
            return await m_repository.InsertAsync(insert);

        }

        public async Task SaveChangesAsync()
        {
            await m_repository.SaveChangesAsync();
        }

        public async Task<TDomain> UpdateAsync(TDomain update)
        {
            return await m_repository.UpdateAsync(update);
        }
    }
}
