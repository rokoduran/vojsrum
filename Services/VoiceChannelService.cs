using System;
using System.Collections.Generic;
using System.Text;
using Database.ViewModels;
using System.Threading.Tasks;
using Repository;
using Database.Models;

namespace Service
{
    public class VoiceChannelService : BaseService<VoiceChannelRepository, VoiceChannel, VoiceChannelView>
    {
        public VoiceChannelService(VoiceChannelRepository repository) : base(repository)
        {
        }
        public async Task<IEnumerable<VoiceChannelView>> GetVoiceChannelsInRoomAsync(Int64 roomId ){
            return await m_repository.GetVoiceChannelsInRoomAsync(roomId);
        }
    }
}