using System;
using System.Collections.Generic;
using System.Text;
using Database.ViewModels;
using System.Threading.Tasks;
using Repository;
using Database.Models;
using Param;

namespace Service
{
    public class MessageService : BaseService<MessageRepository, Message, MessageView>
    {
        public MessageService(MessageRepository repository) : base(repository)
        {
        }

        public async Task<IEnumerable<MessageView>> GetMessagesForTextChannel(Int64 textChannelId, int count){
            return await m_repository.GetMessagesForTextChannel(textChannelId, count);
        }
    }
}