using System;
using System.Collections.Generic;
using Database.ViewModels;
using System.Text;
using System.Threading.Tasks;
using Repository;
using Database.Models;

namespace Service
{
    public class TextChannelService : BaseService<TextChannelRepository, TextChannel, TextChannelView>
    {
        public TextChannelService(TextChannelRepository repository) : base(repository)
        {
        }

         public async Task<IEnumerable<TextChannelView>> GetTextChannelsInRoomAsync(Int64 roomId ){
             return await m_repository.GetTextChannelsInRoomAsync(roomId);
         }

          public async Task<IEnumerable<MessageView>> GetMessagesInTextChannelAsync(Int64 textChannelId){
              return await m_repository.GetMessagesInTextChannelAsync(textChannelId);
          }
    }
}