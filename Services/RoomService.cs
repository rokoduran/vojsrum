using System;
using System.Collections.Generic;
using System.Text;
using Database.ViewModels;
using System.Threading.Tasks;
using Repository;
using Database.Models;
using Param;

namespace Service
{
    public class RoomService : BaseService<RoomRepository, Room, RoomView>
    {
        public RoomService(RoomRepository repository) : base(repository)
        {
        }
        
        public async Task<int> GetCountForFilterAsync(FilteringParam filter, Int64 userId){
            return await m_repository.GetCountForFilterAsync(filter, userId);
        }

        public async Task LeaveRoomAsync(Int64 roomId, Int64 userId){
            await m_repository.LeaveRoomAsync(roomId, userId);
        }

        public async Task<int> GetUserCountForRoomAsync(Int64 roomId){
            return await m_repository.GetUserCountForRoomAsync(roomId);
        }
        public async Task<RoomView> InsertWithUserAsync(RoomView room, Int64 userId){
            return await m_repository.InsertWithUserAsync(room, userId);
        }

        public async Task<bool> JoinRoomAsync(Int64 roomId, Int64 userId){
            return await m_repository.JoinRoomAsync(roomId, userId);
        }

        public async Task<IEnumerable<RoomView>> GetRoomsForParamAsync(ParamObject param){
            return await m_repository.GetRoomsForParamAsync(param);
        }

        public async Task<IEnumerable<RoomView>> GetRoomsForUserAsync(Int64 userId){
            return await m_repository.GetRoomsForUserAsync(userId);
        }
        public async Task<bool> ChangeUserRoleInRoomAsync(Int64 userId, Int64 roomId, UserRole role){
            return await m_repository.ChangeUserRoleInRoomAsync(userId, roomId, role);
        }
    }
}
