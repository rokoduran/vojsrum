using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Database.Models;
using Database.Context;
using Database.ViewModels;
using API.FrontendModels;
using Service;
using Util;
using AutoMapper;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly UserService m_service;
        private readonly VojsrumDbContext m_dbContext;

        private readonly IMapper m_mapper;

        public UserController(VojsrumDbContext dbContext, UserService service, IMapper mapper)
        {
            m_service = service;
            m_dbContext = dbContext;
            m_mapper = mapper;
        }

        [HttpGet]
        [Route("get/users/{roomId}")]
        public async Task<IActionResult> GetUsersForRoom(Int64 roomId)
        {
            var usersForRoom = m_mapper.Map<List<UserFrontendModel>>(await m_service.GetUsersInRoomAsync(roomId));
            foreach(var user in usersForRoom){
                user.UserRole = (await m_service.GetUserRoleInRoomAsync(user.ID,roomId)).ToString();
            }
            return Ok((usersForRoom));
        }

        [HttpGet]
        [Route("get/{userId}")]
        public async Task<IActionResult> GetUser(Int64 userId){
            var user = await m_service.GetByIDAsync(userId);
            if(user == null){
                return NotFound("User doesn't exist!");
            }
            return Ok(m_mapper.Map<UserFrontendModel>(user));
        }

        [HttpPut]
        [Route("put")]
        public async Task<IActionResult> UpdateUser(UserFrontendModel user){
            var updatedUser = await m_service.UpdateAsync(m_mapper.Map<UserView>(user));
            if(updatedUser == null){
                return BadRequest("Requested User doesn't exist.");
            }

            return Ok(m_mapper.Map<UserFrontendModel>(updatedUser));
        }

        [HttpPost]
        [Route("post/auth")]
        public async Task<IActionResult> AuthenticateUser(AuthObj authobj){
            bool success = await m_service.AuthenticateUserAsync(authobj.Username, authobj.Password);
            if(success){
                var userObj = await m_service.GetByUsernameAsync(authobj.Username);
                object frontendObj = new {
                    Id = userObj.ID,
                    Username = userObj.Username
                };
                return Ok(frontendObj);
            } 
            return NotFound("Invalid username/password.");
        }

        [HttpPost]
        [Route("post/create")]
        public async Task<IActionResult> CreateUser(UserFrontendModel user){
            var newUser = await m_service.InsertAsync(m_mapper.Map<UserView>(user));
            if(newUser == null){
                return BadRequest("Failed to create user. Username/mail can't be empty or are taken.");
            }
            return Ok(m_mapper.Map<UserFrontendModel>(newUser));
        }

        [HttpDelete]
        [Route("delete/{userId}")]
        public async Task<IActionResult> DeleteUser(Int64 userId){
            bool result = await m_service.DeleteAsync(userId);
            if(result){
                return Ok("User deleted succesfully.");
            }
            return NotFound("No User was found with the given ID.");

        }
    }
}
