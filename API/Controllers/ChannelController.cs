using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;
using Database.ViewModels;
using Database.Context;
using API.FrontendModels;
using API.Hubs;
using Service;
using Param;
using AutoMapper;
using Util;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ChannelController : ControllerBase
    {
        private readonly TextChannelService m_textChannelService;

        private readonly VoiceChannelService m_voiceChannelService;

        private readonly MessageService m_messageService;

        private readonly IHubContext<GeneralHub> m_generalHub;

        private readonly IHubContext<TextHub> m_textHub;
        private readonly VojsrumDbContext m_dbContext;

        private readonly IMapper m_mapper;

        public ChannelController(VojsrumDbContext dbContext, TextChannelService textChannelService, 
        VoiceChannelService voiceChannelService, MessageService messageService, IHubContext<GeneralHub> generalHub, IHubContext<TextHub> textHub, IMapper mapper)
        {
            m_textChannelService = textChannelService;
            m_voiceChannelService = voiceChannelService;
            m_messageService = messageService;
            m_dbContext = dbContext;
            m_generalHub = generalHub;
            m_textHub = textHub;

            m_mapper = mapper;
        }

        [HttpGet]
        [Route("get/text/{channelId}")]
        public async Task<IActionResult> GetTextChannel(Int64 channelId)
        {
            var textChannel = await m_textChannelService.GetByIDAsync(channelId);
            if(textChannel == null){
                return NotFound("Text channel doesn't exist!");
            }
            return Ok(m_mapper.Map<TextChannelFrontendModel>(textChannel));
        }

        [HttpGet]
        [Route("get/voice/{channelId}")]
        public async Task<IActionResult> GetVoiceChannel(Int64 channelId)
        {
            var voiceChannel = await m_voiceChannelService.GetByIDAsync(channelId);
            if(voiceChannel == null){
                return NotFound("Voice channel doesn't exist!");
            }
            return Ok(m_mapper.Map<VoiceChannelFrontendModel>(voiceChannel));
        }

        [HttpGet]
        [Route("get/text/all/{roomId}")]
        public async Task<IActionResult> GetTextChannelsInRoom(Int64 roomId)
        {
            var textChannels = await m_textChannelService.GetTextChannelsInRoomAsync(roomId);
            return Ok(m_mapper.Map<List<TextChannelFrontendModel>>(textChannels));
        }

        [HttpGet]
        [Route("get/voice/all/{roomId}")]
        public async Task<IActionResult> GetVoiceChannelsInRoom(Int64 roomId)
        {
            var voiceChannels = await m_voiceChannelService.GetVoiceChannelsInRoomAsync(roomId);
            return Ok(m_mapper.Map<List<VoiceChannelFrontendModel>>(voiceChannels));
        }

        [HttpGet]
        [Route("get/text/messages/{textChannelId}")]
        public async Task<IActionResult> GetMessagesInTextChannel(Int64 textChannelId){
            return Ok(m_mapper.Map<List<MessageFrontendModel>>(await m_textChannelService.GetMessagesInTextChannelAsync(textChannelId)));
        }


        [HttpPut]
        [Route("put/text")]
        public async Task<IActionResult> UpdateTextChannel(TextChannelFrontendModel channel){
            var updatedChannel = await m_textChannelService.UpdateAsync(m_mapper.Map<TextChannelView>(channel));
            if(updatedChannel == null){
                return BadRequest("Requested text channel doesn't exist.");
            }
            var channelFrontend = m_mapper.Map<TextChannelFrontendModel>(updatedChannel);
            await m_generalHub.Clients.Groups("r" + channel.RoomId.ToString()).SendAsync("UpdatedTextChannelEvent", channelFrontend);
            return Ok(channelFrontend);
        }

        [HttpPut]
        [Route("put/voice")]
        public async Task<IActionResult> UpdateVoiceChannel(VoiceChannelFrontendModel channel){
            var updatedChannel = await m_voiceChannelService.UpdateAsync(m_mapper.Map<VoiceChannelView>(channel));
            if(updatedChannel == null){
                return BadRequest("Requested voice channel doesn't exist.");
            }

            var channelFrontend = m_mapper.Map<VoiceChannelFrontendModel>(updatedChannel);
            await m_generalHub.Clients.Groups("r" + channel.RoomId.ToString()).SendAsync("UpdatedVoiceChannelEvent", channelFrontend);
            return Ok(channelFrontend);
        }

        [HttpPost]
        [Route("post/text/create")]
        public async Task<IActionResult> CreateTextChannel(TextChannelFrontendModel channel){
            var newChannel = await m_textChannelService.InsertAsync(m_mapper.Map<TextChannelView>(channel));
            if(newChannel == null){
                return BadRequest("Failed to create channel. Another text channel with the same name already exists in the room.");
            }
            var newChannelFrontend = m_mapper.Map<TextChannelFrontendModel>(newChannel);
            await m_generalHub.Clients.Groups("r" + channel.RoomId.ToString()).SendAsync("NewTextChannelEvent", newChannelFrontend);
            return Ok(newChannelFrontend);
        }

        [HttpPost]
        [Route("post/voice/create")]
        public async Task<IActionResult> CreateVoiceChannel(VoiceChannelFrontendModel channel){
            var newChannel = await m_voiceChannelService.InsertAsync(m_mapper.Map<VoiceChannelView>(channel));
            if(newChannel == null){
                return BadRequest("Failed to create channel. Another voice channel with the same name already exists in the room.");
            }
            var newChannelFrontend = m_mapper.Map<VoiceChannelFrontendModel>(newChannel);
            await m_generalHub.Clients.Groups("r" + channel.RoomId.ToString()).SendAsync("NewVoiceChannelEvent", newChannelFrontend);
            return Ok(newChannelFrontend);
        }

        [HttpDelete]
        [Route("delete/text/{roomId}/{channelId}")]
        public async Task<IActionResult> DeleteTextChannel(Int64 roomId, Int64 channelId){
            bool result = await m_textChannelService.DeleteAsync(channelId);
            if(result){
                await m_generalHub.Clients.Groups("r" + roomId.ToString()).SendAsync("TextChannelDeletedEvent", channelId);
                return Ok("Text channel deleted succesfully.");
            }
            return NotFound("No Text channel was found with the given ID.");

        }

        [HttpDelete]
        [Route("delete/voice/{roomId}/{channelId}")]
        public async Task<IActionResult> DeleteVoiceChannel(Int64 roomId, Int64 channelId){
            bool result = await m_voiceChannelService.DeleteAsync(channelId);
            if(result){
                await m_generalHub.Clients.Groups("r" + roomId.ToString()).SendAsync("VoiceChannelDeletedEvent", channelId);
                return Ok("Voice channel deleted succesfully.");
            }
            return NotFound("No Voice channel was found with the given ID.");

        }
        [HttpPost]
        [Route("post/enter/{connectionId}/{textChannelId}")]
        public async Task<IActionResult> EnterTextChannel(string connectionId, Int64 textChannelId){
            var obj = PresentUsersDict.Get(connectionId);
            obj.TextChannelId = textChannelId;
            PresentUsersDict.Update(connectionId, obj);
            await m_generalHub.Groups.AddToGroupAsync(connectionId, "t" + textChannelId);
            return Ok(connectionId + " added to group.");
        }
        [HttpPost]
        [Route("post/exit/{connectionId}/{textChannelId}")]
        public async Task<IActionResult> ExitTextChannel(string connectionId, Int64 textChannelId){
            var obj = PresentUsersDict.Get(connectionId);
            obj.TextChannelId = 0;
            PresentUsersDict.Update(connectionId, obj);
            await m_generalHub.Groups.RemoveFromGroupAsync(connectionId, "t" + textChannelId);
            return Ok(connectionId + " removed from group.");
        }

        [HttpGet]
        [Route("get/messages/{textChannelId}/{messageCount}")]
        public async Task<IActionResult> GetMessages(Int64 textChannelId, int messageCount){
            var result = m_mapper.Map<List<MessageFrontendModel>>(await m_messageService.GetMessagesForTextChannel(textChannelId, messageCount));
            if(result.Count() == 0){
                return BadRequest("No messages left in the channel.");
            }
            int newCount = messageCount + result.Count();
            
            return Ok(new {
                messages = result,
                count = newCount
            });
        }
        [HttpPost]
        [Route("post/sendmessage")]
        public async Task<IActionResult> SendMessage(MessageFrontendModel message){
            var result = await m_messageService.InsertAsync(m_mapper.Map<MessageView>(message));
            if(result != null){
                var mapperResult = m_mapper.Map<MessageFrontendModel>(result);
                object newMessage = new {
                    ID = result.ID,
                    DateCreated = result.DateCreated.ToString("dd/MM/yyyy HH:mm:ss"),
                    TextChannelId = result.TextChannelId,
                    MessageType = result.MessageType,
                    MessageData = result.MessageData,
                    UserId = result.UserId
                };
                await m_generalHub.Clients.Groups("t" + message.TextChannelId).SendAsync("NewMessageEvent", newMessage);
                return Ok();
            }
            return BadRequest("Message sent was not correctly set up.");
        }
    }
}
