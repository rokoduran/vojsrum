using AutoMapper;
using Database.Models;
using Database.ViewModels;
using API.FrontendModels;

namespace API.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserView>().ReverseMap();
            CreateMap<Room, RoomView>().ReverseMap();
            CreateMap<TextChannel, TextChannelView>().ReverseMap();
            CreateMap<VoiceChannel, VoiceChannelView>().ReverseMap();
            CreateMap<Message, MessageView>().ReverseMap();
            CreateMap<UserFrontendModel, UserView>().ReverseMap();
            CreateMap<RoomFrontendModel, RoomView>().ReverseMap();
            CreateMap<TextChannelFrontendModel, TextChannelView>().ReverseMap();
            CreateMap<VoiceChannelFrontendModel, VoiceChannelView>().ReverseMap();
            CreateMap<MessageFrontendModel, MessageView>().ReverseMap();
        }
    }
}