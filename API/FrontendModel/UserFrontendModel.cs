using System;
using Database.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.FrontendModels
{
    public class UserFrontendModel{
        public Int64 ID { get; set; }
        public string Username {get; set;}
        
        public string Mail { get; set; }
        public string Password { get; set; }
        public string ProfilePictureLink { get; set; }
        public string UserRole { get; set; }
    }
}