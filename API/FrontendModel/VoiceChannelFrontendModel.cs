using System;
using Database.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.FrontendModels
{
    public class VoiceChannelFrontendModel{
        public Int64 ID { get; set; }
        public string Name { get; set; }
        public Int64 RoomId { get; set; }
    }
}