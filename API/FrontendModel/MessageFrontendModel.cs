using System;
using Database.Models;
using Database.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.FrontendModels
{
    public class MessageFrontendModel{
        public Int64 ID { get; set; }
        public Int64 UserId { get; set; }
        public string MessageData { get; set; }
        public MessageType MessageType { get; set; }
        [Required]public DateTime DateCreated { get; set; }
        public Int64 RoomId {get; set;}
        public Int64 TextChannelId { get; set; }
    }
}