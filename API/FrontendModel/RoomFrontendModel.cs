using System;
using Database.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.FrontendModels
{
    public class RoomFrontendModel{
        public Int64 ID { get; set; }
        public string Name { get; set; }

        public int Population { get; set; }
    }
}