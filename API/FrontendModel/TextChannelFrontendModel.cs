using System;
using Database.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.FrontendModels
{
    public class TextChannelFrontendModel{
        public Int64 ID { get; set; }
        
        public string BannerLink { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public Int64 RoomId { get; set; }
    }
}