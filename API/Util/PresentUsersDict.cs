using System.Collections.Generic;
using System;

namespace Util{
    public static class PresentUsersDict  // no (collection) base class
    {
    private static Dictionary<string, DictObj> m_dict =  new Dictionary<string,DictObj>();

    public static bool Add(string k, DictObj v)
    {
        // can be a optimized a little with TryGetValue, this is for clarity
        if (m_dict.ContainsKey(k)){
                return false;
            }

        else{
            m_dict.Add(k, v);
            return true;
        }
    }

    public static bool Update(string k, DictObj v){
        if(m_dict.ContainsKey(k)){
            m_dict[k] = v;
            return true;
        }
        return false;
    }

    public static void Remove(string k)
    {
        if(m_dict.ContainsKey(k))
            m_dict.Remove(k);
    }
    public static DictObj Get(string k){
        if(m_dict.ContainsKey(k))
            return m_dict[k];
        else 
            return null;
    }

    public static List<Int64> GetUserIdsForRoom(Int64 roomId){
        List<Int64> ids = new List<Int64>();
        foreach(var pair in m_dict){
            if(pair.Value.RoomId == roomId){
                ids.Add(pair.Value.UserID);
            }
        }
        return ids;
    }
    // more members
    }
}