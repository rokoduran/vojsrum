using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Database.Context;
using Repository;
using Service;
using Util;
using API.Hubs;
using API.Mapping;
using AutoMapper;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<VojsrumDbContext>();
            services.AddTransient<RoomRepository>();
            services.AddTransient<UserRepository>();
            services.AddTransient<TextChannelRepository>();
            services.AddTransient<VoiceChannelRepository>();
            services.AddTransient<MessageRepository>();
            services.AddTransient<RoomService>();
            services.AddTransient<UserService>();
            services.AddTransient<TextChannelService>();
            services.AddTransient<VoiceChannelService>();
            services.AddTransient<MessageService>();
            services.AddSignalR();
            var mapperConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new Mapping.MappingProfile());
                });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
                    
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCookiePolicy();
            app.UseCors(builder => builder
            .WithOrigins("https://26.157.162.1:3000")
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            );
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<GeneralHub>("/hubs/general");
                endpoints.MapHub<TextHub>("/hubs/text");
                endpoints.MapControllers();
            });
        }
    }
}
