using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Hubs
{
    public class TextHub: Hub
    {
        public string GetConnectionId() => Context.ConnectionId;
    }
}
