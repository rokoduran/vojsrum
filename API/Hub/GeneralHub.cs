using Microsoft.AspNetCore.SignalR;
using Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Hubs
{
    public class GeneralHub: Hub
    {
        public string GetConnectionId() => Context.ConnectionId;

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            Console.WriteLine("attempted to disconnect " + Context.ConnectionId);
            var obj = PresentUsersDict.Get(Context.ConnectionId);
            if(obj!= null){
                if(obj.VoiceChannelId > 0){
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, "v" + obj.VoiceChannelId.ToString());
                    await Clients.Group("v" + obj.VoiceChannelId.ToString()).SendAsync("VoiceChannelExitEvent", obj.UserID);
                }

                if(obj.TextChannelId > 0){
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, "t" + obj.TextChannelId.ToString());
                    await Clients.Group("t" + obj.TextChannelId.ToString()).SendAsync("TextChannelExitEvent", obj.UserID);
                }

                if(obj.RoomId > 0){
                    await Groups.RemoveFromGroupAsync(Context.ConnectionId, "r" + obj.RoomId.ToString());
                    await Clients.Group("r" + obj.RoomId.ToString()).SendAsync("RoomExitEvent", obj.UserID);
                }
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, "GeneralGroup");
                PresentUsersDict.Remove(Context.ConnectionId);
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}
