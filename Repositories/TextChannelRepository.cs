using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database.Models;
using Database.ViewModels;
using Database.Context;
using Microsoft.EntityFrameworkCore;

namespace Repository{
    public class TextChannelRepository: BaseRepository<TextChannel, TextChannelView>{
        public TextChannelRepository(VojsrumDbContext context, IMapper mapper): base(context, mapper){

        }

        public override async Task<TextChannelView> InsertAsync(TextChannelView textchannel){
            if(textchannel.Name.Length == 0){
                return null;
            }
            var existing = await (m_dbContext.TextChannel
            .Include(x => x.Room)
            .Where(x => x.RoomId == textchannel.RoomId)
            .FirstOrDefaultAsync(x => x.Name == textchannel.Name));

            if(existing!= null){
                return null;
            }
           return m_mapper.Map<TextChannelView>(await base.InsertAsync(textchannel));
        }

        public async Task<IEnumerable<TextChannelView>> GetTextChannelsInRoomAsync(Int64 roomId ){
            return m_mapper.Map<List<TextChannelView>>(await m_dbContext.TextChannel
            .Include(x => x.Room)
            .Where(x => x.Room.TextChannels.Any(y => y.RoomId == roomId))
            .ToListAsync());
        }

        public async Task<IEnumerable<MessageView>> GetMessagesInTextChannelAsync(Int64 textChannelId){
            return m_mapper.Map<List<MessageView>>(await m_dbContext.Message
            .Include(x => x.TextChannel)
            .Where(x => x.TextChannelId == textChannelId)
            .ToListAsync()); 
        }
        
    }
}