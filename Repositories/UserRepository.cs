using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Database.Models;
using Database.ViewModels;
using AutoMapper;
using Database.Context;
using Microsoft.EntityFrameworkCore;
using BCrypt.Net;

namespace Repository{
    public class UserRepository: BaseRepository<User,UserView>{
        public UserRepository(VojsrumDbContext context, IMapper mapper): base(context, mapper){

        }

        public override async Task<UserView> InsertAsync(UserView user){
            if(user.Username.Length == 0 || user.Mail.Length == 0){
                return null;
            }
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
            var existing = await (m_dbContext.User.FirstOrDefaultAsync(x => x.Username == user.Username));

            if(existing!= null){
                return null;
            }
           return await base.InsertAsync(user);
        }

        public async Task<UserView> GetByUsernameAsync(string username){
            return m_mapper.Map<UserView>(await m_dbContext.User
                .FirstOrDefaultAsync(x => x.Username == username));
        }

        public async Task<bool> AuthenticateUserAsync(string username, string password){
            User user = m_mapper.Map<User>(await GetByUsernameAsync(username));
            if(user == null || !BCrypt.Net.BCrypt.Verify(password, user.Password)){
                return false;
            }
            return true;
        }

        public async Task<IEnumerable<UserView>> GetUsersInRoomAsync(Int64 roomId ){
            return m_mapper.Map<List<UserView>>(await m_dbContext.User
                .Include(x => x.Rooms)
                .ThenInclude( x => x.Room)
                .Where(x => x.Rooms.Any(y => y.RoomId == roomId))
                .ToListAsync());
        }
        public async Task BanUserInRoomAsync(Int64 userId, Int64 roomId){
            var userToBan = await m_dbContext.RoomUser
                .FirstAsync(x => x.UserId == userId && x.RoomId == roomId);
            userToBan.UserRole = UserRole.Banned;

            await SaveChangesAsync();
        }

        public async Task<UserRole> GetUserRoleInRoomAsync(Int64 userId, Int64 roomId){
            return (await m_dbContext.RoomUser.FirstAsync(x => x.UserId == userId && x.RoomId == roomId)).UserRole;
        }
        public async Task PromoteUserInRoomAsync(Int64 userId, Int64 roomId){
            var userToBan = await m_dbContext.RoomUser
                .FirstAsync(x => x.UserId == userId && x.RoomId == roomId);
            if(userToBan.UserRole != UserRole.Admin){
                UserRole role = userToBan.UserRole;
                role++;
                userToBan.UserRole = role;
            }

            await SaveChangesAsync();
        }
    }
}