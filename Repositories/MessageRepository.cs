using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Database.Models;
using Database.ViewModels;
using Database.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Param;

namespace Repository{
    public class MessageRepository: BaseRepository<Message,MessageView>{
        public MessageRepository(VojsrumDbContext context, IMapper mapper): base(context, mapper){

        }
     public async Task<IEnumerable<MessageView>> GetMessagesForTextChannel(Int64 textChannelId, int count){
            var query = m_dbContext.Set<Message>().Where(x => x.TextChannelId == textChannelId).AsQueryable();
            query = query.OrderByDescending(x => x.DateCreated);
            var messageList = (await query.Skip(count).Take(PagingParam.PageSize).ToListAsync());

            return m_mapper.Map<List<MessageView>>(messageList);
        }

    }
}