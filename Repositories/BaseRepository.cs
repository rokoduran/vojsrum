using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Database.Common;
using Database.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    //Generic repository pattern.
    //Generic interface repo for all CRUD operations
    public abstract class BaseRepository<TData, TDomain> 
        where TData : class, IBase
        where TDomain : class, IBase
    {
        protected readonly VojsrumDbContext m_dbContext;
        protected readonly IMapper m_mapper;

        protected BaseRepository(VojsrumDbContext dbContext, IMapper mapper)
        {
            m_dbContext = dbContext;
            m_mapper = mapper;
        }

        public async Task<bool> DeleteAsync(Int64 id)
        {
            var e = await m_dbContext.Set<TData>().FindAsync(id);
            if (e != null)
            {
                m_dbContext.Set<TData>().Attach(e);
                m_dbContext.Entry(e).State = EntityState.Deleted;
                await SaveChangesAsync();
                return true;
            }
            return false;
        }
        //Override in derived repos. Can only work with paging as it's unknown which specific data set is in question for other params.
        /*public virtual async Task<IEnumerable<TType>> GetAllForParamsAsync(FilteringParam filter, PagingParam paging, SortingParam sort)
        {
            var query = m_dbContext.Set<TType>().AsQueryable();
            return mapper.Map<List<TType>>(await query.Skip((paging.currentPage - 1) * PagingParam.PageSize).Take(PagingParam.PageSize).ToListAsync());
        }*/

        public async Task<TDomain> GetByIDAsync(Int64 id)
        {
           return m_mapper.Map<TDomain>(await m_dbContext.Set<TData>().AsNoTracking().SingleOrDefaultAsync(x => x.ID == id));
        }

        public virtual async Task<int> GetCountAsync()
        {
            return (await m_dbContext.Set<TData>().ToListAsync()).Count;
        }

        public virtual async Task<TDomain> InsertAsync(TDomain insert)
        {
            insert.DateCreated = DateTime.Now;
            insert.DateUpdated = DateTime.Now;
            var newEntry = m_dbContext.Set<TData>().Add(m_mapper.Map<TData>(insert));

            await SaveChangesAsync();
            return m_mapper.Map<TDomain>(newEntry.Entity);
        }

        public async Task SaveChangesAsync()
        {
            await m_dbContext.SaveChangesAsync();
        }

        public async Task<TDomain> UpdateAsync(TDomain update)
        {
            var entity = m_mapper.Map<TData>(update);
            entity.DateUpdated = DateTime.Now;
            TData existing = m_dbContext.Set<TData>().Find(entity.ID);

            if (existing != null){
                m_dbContext.Entry(existing).CurrentValues.SetValues(entity);
                await SaveChangesAsync();
            }
            return m_mapper.Map<TDomain>(existing);
        }
    }
}