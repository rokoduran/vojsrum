using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Database.Models;
using Database.ViewModels;
using Database.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Param;

namespace Repository{
    public class RoomRepository: BaseRepository<Room,RoomView>{
        public RoomRepository(VojsrumDbContext context, IMapper mapper): base(context, mapper){

        }

        public override async Task<RoomView> InsertAsync(RoomView room){
            if(room.Name.Length == 0){
                return null;
            }

           return await base.InsertAsync(room);
        }

        public async Task<int> GetUserCountForRoomAsync(Int64 roomId){
            return (await m_dbContext.User
            .Include(x => x.Rooms)
            .Where( 
                x => x.Rooms
                .Any( x => x.RoomId == roomId && x.UserRole != UserRole.Banned)
            )
            .ToListAsync())
            .Count();
        }

        public async Task LeaveRoomAsync(Int64 roomId, Int64 userId){
            m_dbContext.RoomUser.Remove(
                await m_dbContext.RoomUser.FirstAsync(X => X.RoomId == roomId && X.UserId == userId)
            );
            await SaveChangesAsync();
            if(m_dbContext.RoomUser.Where(x => x.RoomId == roomId).ToList().Count() == 0){
                await DeleteAsync(roomId);
                await SaveChangesAsync();
            }
        }
        
        public async Task<RoomView> InsertWithUserAsync(RoomView room, Int64 userId){
            var result = await InsertAsync(room);
            if(result == null){
                return result;
            }

            RoomUser roomUser = new RoomUser
            {
                RoomId = result.ID,
                UserId = userId,
                UserRole = UserRole.Admin
            };

            m_dbContext.RoomUser.Add(roomUser);
            await m_dbContext.SaveChangesAsync();
            return result;
        }

        public async Task<int> GetCountForFilterAsync(FilteringParam filter, Int64 userId){
            return (await (await GetForFilterAsync(filter, userId)).ToListAsync()).Count();
        }
        private async Task<IQueryable<Room>> GetForFilterAsync(FilteringParam filtering, Int64 userId){
            var unbannedUserRoomList =await m_dbContext.Set<RoomUser>()
                        .Where(x => x.UserId == userId)
                        .ToListAsync();

            List<Int64> uniqueRoomIdList = new List<Int64>();
            foreach(var roomUser in unbannedUserRoomList){
                if(!uniqueRoomIdList.Contains(roomUser.RoomId)){
                    uniqueRoomIdList.Add(roomUser.RoomId);
                }
            };

            var query = m_dbContext.Set<Room>()
                .Where(x => !uniqueRoomIdList.Contains(x.ID))
                .AsQueryable();
                

            if(filtering.filterType == "Name"){
                    query = query.Where(x => x.Name.ToLower().Contains(filtering.filter));
            }
            return query;
        }


        public async Task<bool> IsUserBannedInRoomAsync(Int64 roomId, Int64 userId){

           return await m_dbContext.Room
                        .Include(x => x.RoomUsers)
                        .AnyAsync(x => x.RoomUsers.Any(y => y.UserId == userId && y.RoomId==roomId && y.UserRole == UserRole.Banned));
        }   

        public async Task<bool> JoinRoomAsync(Int64 roomId, Int64 userId){
            if(await IsUserBannedInRoomAsync(roomId, userId)){
                return false;
            }
            RoomUser roomUser = new RoomUser
            {
                RoomId = roomId,
                UserId = userId,
                UserRole = UserRole.Guest
            };

            m_dbContext.RoomUser.Add(roomUser);
            await m_dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<RoomView>> GetRoomsForParamAsync(ParamObject param){
            //Filtering
            var query = await GetForFilterAsync(param.filtering, param.userId);
            //Sorting
            bool reverseOrder = param.sorting.sort == SortingParam.SortSettings.Item2;
            if(param.sorting.orderBy == "Name")
            {
                query = reverseOrder ? query.OrderByDescending(x => x.Name) : query.OrderBy(x => x.Name);
            }
            //Paging and end
            var roomList = (await query.Skip((param.paging.currentPage - 1) * PagingParam.PageSize).Take(PagingParam.PageSize).ToListAsync());

            return m_mapper.Map<List<RoomView>>(roomList);
        }

        public async Task<IEnumerable<RoomView>> GetRoomsForUserAsync(Int64 userId){
            return  m_mapper.Map<List<RoomView>>(await m_dbContext.Room
            .Include(x => x.RoomUsers)
            .Where(x => x.RoomUsers.Any(y => y.UserId == userId && y.UserRole != UserRole.Banned))
            .ToListAsync());
        }
        public async Task<bool> ChangeUserRoleInRoomAsync(Int64 userId, Int64 roomId, UserRole role){
            var roomUser = await m_dbContext.RoomUser
                .FirstOrDefaultAsync(x => x.UserId == userId && x.RoomId == roomId);

            if(roomUser == null){
                return false;
            }
            roomUser.UserRole = role;
            await SaveChangesAsync();

            return true;
        }
        
    }
}