using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database.Models;
using Database.ViewModels;
using Database.Context;
using Microsoft.EntityFrameworkCore;

namespace Repository{
    public class VoiceChannelRepository: BaseRepository<VoiceChannel, VoiceChannelView>{
        public VoiceChannelRepository(VojsrumDbContext context, IMapper mapper): base(context, mapper){

        }

        public override async Task<VoiceChannelView> InsertAsync(VoiceChannelView voiceChannel){
            if(voiceChannel.Name.Length == 0){
                return null;
            }
            var existing = await (m_dbContext.voiceChannel
            .Include(x => x.Room)
            .Where(x => x.RoomId == voiceChannel.RoomId)
            .FirstOrDefaultAsync(x => x.Name == voiceChannel.Name));

            if(existing!= null){
                return null;
            }
           return await base.InsertAsync(voiceChannel);
        }

        public async Task<IEnumerable<VoiceChannelView>> GetVoiceChannelsInRoomAsync(Int64 roomId ){
            return m_mapper.Map<List<VoiceChannelView>>(await m_dbContext.voiceChannel
            .Include(x => x.Room)
            .Where(x => x.Room.VoiceChannels.Any(y => y.RoomId == roomId))
            .ToListAsync());
        }
    }
}