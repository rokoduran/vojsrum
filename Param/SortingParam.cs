﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Param
{
    public class SortingParam
    {
        public static readonly Tuple<string, string> SortSettings = new Tuple<string, string>("Asc", "Desc");

        public string sort { get; set; } = SortSettings.Item1;
        public string orderBy { get; set; }
    }
}
