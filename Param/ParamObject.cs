﻿using System;

namespace Param
{
    public class ParamObject
    {
        public PagingParam paging { get; set; }
        public FilteringParam filtering { get; set; }
        public SortingParam sorting { get; set; }

        public Int64 userId {get; set;}
    }
}
