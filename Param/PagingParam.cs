﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Param
{
    public class PagingParam
    {
        public static int PageSize = 15;
        public int currentPage { get; set; }
        public int totalPages { get; set; }
    }
}
